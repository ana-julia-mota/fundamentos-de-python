# Fundamentos de python

## Ativar ambiente virtual conda

``` 
git pull origin main && conda activate env_python_anajulia && conda env update
```


## Criar ambiente virtual Conda

```
conda env create -f environment.yml
```
## Tarefas:
- 16/11/2021
    - Arrumar as informações, deixar mais amigável, parte dos "input".
    - Integrar os outros sabores e informações.
    - Tentar colocar os inputs no While (rodar os inputs de maneira infinita).
    - Integrar return, break, continue e try/except.

- 09/12/2021
    - Aplicar o desconto.
    - Listar o pedido final;Indicar os valores de cada elemento.
    - Brindes.
    - Tempo de espera.
