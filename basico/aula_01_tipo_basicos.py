'''
Tipos de dados básicos

- String (str)
- Números(Eles podem ser inteiros(int), decimais(float) ou complexos.)
'''

# Operações com Strings
nome = "Ana"
data = "26/10/2021"
projeto = "Hemeroteca-peb"
frase = "Operações com Strings"

print(nome + " " + data + " " + projeto)
print(f'{nome} {data} {projeto}') # f string
print(nome*3)

print(frase[-7:])

for caracter in frase: 
    print(caracter)



#num_x = int(input('Digite um número: '))
#num_y = int(input('Digite um número: '))
num_x, num_y = [int(x) for x in  input('Digite dois números: ').split()]
#Soma
soma = int(num_x) + num_y
print(f' O valor da soma dos números escolhidos é: {soma} ')

#Subtração
subtracao = int(num_x) - num_y  
print(subtracao, type(subtracao))
print(f'A subtração de {num_x} - {num_y} é igual a: {subtracao}')
print(f'O tipo de dado é {type(subtracao)} ')


#Multiplicação
multiplicacao = num_x * num_y
print(multiplicacao, type(multiplicacao))
print(f'A multiplicação de {num_x} * {num_y} é igual a : {multiplicacao} ')
print(f'O tipo de dado é {type(multiplicacao)} ')



