from functools import reduce

class Pizza: 
    def __init__(self, sabor='muçarela', formato=1, bebida='guaraná'):
        if formato == 'redonda' or formato ==8:
            self.formato = 8
        elif formato =='quadrada' or formato == 12:
            self.formato = 12
        else:
            self.formato = int(formato)
        self.sabor = sabor
        bebida.lower()
        self.bebida = bebida
        self.desconto = 0.5
    
    def preco_bebida (self):
        if self.bebida == 'Coca-cola - 2L' or self.bebida=='1': 
            return 8.60

        elif self.bebida == 'Guaraná - 2L' or self.bebida=='2':
            return 7.00

        elif self.bebida == 'Suco de laranja - 1,5L' or self.bebida=='3':
            return 6.50

        elif self.bebida == 'Suco de uva - 1L' or self.bebida=='4':
            return 6.80

    def preco_pizza (self):
        valor = self.formato*5.50
        return valor
        
        
class Muçarela(Pizza): 
    def __init__(self, sabor, formato, bebida):
        super().__init__(sabor, formato, bebida)
        
    def ingredientes_geral (self):
        ingredientes = '\n Ingredientes \n Muçarela \n Molho de tomate \n Tomate \n Orégano \n'
        return ingredientes

    def preco_pizza (self):
        if self.formato == 'redonda' or self.formato=='1':
            return 45.0

        if self.formato == 'quadrada' or self.formato=='2':
            return 66.0

        if self.formato == 'pedaço' or self.formato=='3':
            return 6.0



class Calabresa(Pizza):
    def __init__(self, sabor, formato, bebida):
        super().__init__(sabor, formato, bebida)

    def ingredientes_geral (self):
        ingredientes = '\n Ingredientes: \n Calabresa \n Muçarela \n Molho de tomate \n Orégano \n Cebola \n'
        return ingredientes  

    def preco_pizza (self):
        if self.formato == 8:
            return 25.0

        elif self.formato == 12:
            return 90.0

        else:
            return self.formato*9

          

class Frango(Pizza):
    def __init__(self, sabor, formato, bebida):
        super().__init__(sabor, formato, bebida)

    def ingredientes_geral (self):
        ingredientes = '\n Ingredientes: \n Frango \n Muçarela \n Molho de tomate \n Catupiry \n'
        return ingredientes

class Lombinho(Pizza):
    def __init__(self, sabor, formato, bebida):
        super().__init__(sabor, formato, bebida)

    def ingredientes_geral (self):
        ingredientes = '\n Lombinho \n Muçarela \n Molho de tomate \n Catupiry \n'
        return ingredientes




def main(): 
    pizzas = [('Muçarela', 20.0, 66.0, 6.0), ('Calabresa', 25.0, 90.0 ,9.0), ('Frango', 66.0, 73.0, 7.0 ), ('Lombinho', 54.90, 64.9, 7.5)]
    bebidas = [('Coca-cola - 2L', 8.60), ('Guaraná - 2L', 7.0), ('Suco de laranja - 1,5L', 6.5), ('Suco de uva - 1L', 6.8)]
    brindes = ['Chaveiro', 'Caneta', 'Copo', 'Imã de geladeira']
    formatos = ['Redonda(8 pedaços)', 'Quadrada(12 pedaços)', 'Pedaço(1 pedaço)', '2 pedaços', '3 pedaços']
    print(f'\n Cardápio: \n')
    for index,(sabor,brinde,bebida,formato) in enumerate(zip(pizzas,brindes,bebidas,formatos),start=1):
        print(f'{index} - {sabor[0]} - {formatos[0]} - {sabor[1]} | {formatos[1]} - {sabor[2]} | {formatos[2]} - {sabor[3]} | Brindes: {brinde}')
    print('-----')
    for index,bebida in enumerate(bebidas, start=1):
        print(f'{index} - {bebida[0]} - Preço: {bebida[1]}')
    
    menor_preco = input('Gostaria de saber a pizza com o menor valor?').lower()
    if menor_preco == 'sim':
        pizza = [pizzas[0][1],pizzas[1][1],pizzas[2][1],pizzas[3][1]]
        preco = reduce((lambda x,y: x if(x < y) else y), pizza)
        print(f'O sabor {pizzas[pizza.index(preco)][0]} tem o menor valor: {preco}')

    maior_preco = input('Gostaria de saber a pizza com o maior valor?').lower()
    if maior_preco == 'sim':
        pizza = [pizzas[0][1],pizzas[1][1],pizzas[2][1],pizzas[3][1]]
        preco = reduce((lambda x,y: x if(x > y) else y), pizza)
        print(f'O sabor {pizzas[pizza.index(preco)][0]} tem o maior valor: {preco}')
   


    sabor = input('\nEscolha o sabor da pizza:').lower()
    #for pedaco in formatos:
        #print(pedaco)
    formato = input('\nEscolha o formato: \n').lower()
    #for index,bebida in enumerate (bebidas,start=1):
     #   print(f'{index} - {bebida}')
    bebida = input('\nEscolha uma a bebida: \n').lower()
    #for index,brinde in enumerate (brindes,start=1):
        #print(f'{index} - {brinde}')
    #brinde = input('\nEscolha um brinde: \n').lower()

        
    if sabor == 'muçarela' or sabor=='1':
        sabor = 'muçarela'
        pedido = Muçarela(sabor, formato, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
    elif sabor == 'calabresa' or sabor=='2':
        sabor = 'calabresa'
        pedido = Calabresa(sabor, formato, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
        
    elif sabor == 'frango' or sabor=='3':
        pedido = Frango(sabor, formato, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    

    elif sabor == 'lombinho' or sabor=='3':
        pedido = Lombinho(sabor, formato, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
      

    elif sabor == 'marguerita' or sabor=='4':
        pedido = Marguerita(sabor, formato, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
        
       
       
   

    
if __name__ == "__main__":
        main()

    
    

