# Exercício loop

'''def loop_for ():
    pizzas = ['muçarela', 'calabresa', 'frango', 'lombinho']
    bebidas = ['guaraná', 'coca-cola', 'suco de laranja', 'suco de uva']
    brindes = ['chaveiro', 'caneta', 'copo', 'imã de geladeira']
    print(type(pizzas))
    print('\n O sabores de pizzas que possuímos são:')
    for pizza in pizzas:
        print(pizza)
    print('\n O brindes disponíveis são:')
    for pizza in brindes:
        print(pizza)
    print('\n As bebidas que possuímos são:')
    for pizza in bebidas:
        print(pizza)
    pizzas.append('4 queijos')
    print(pizzas)
    for combo in zip(pizzas, bebidas, brindes):
        print(combo)

def main ():
    loop_for()
    #loop_while()

if __name__ == "__main__": 
    main ()'''


# Exercício pizzaria

class Pizza:
    def __init__(self, sabor, qtd_pedacos, bebida):
        self.qtd_pedacos = qtd_pedacos
        self.sabor = sabor
        bebida.lower()
        self.bebida = bebida

    def preco_bebida (self):
        if self.bebida == 'Coca-cola - 2L' or self.bebida=='1': 
            return 8.60

        elif self.bebida == 'Guaraná - 2L' or self.bebida=='2':
            return 7.00

        elif self.bebida == 'Suco de laranja - 1,5L' or self.bebida=='3':
            return 6.50

        elif self.bebida == 'Suco de uva - 1L' or self.bebida=='4':
            return 6.80

    def preco_pizza (self):
        valor = self.qtd_pedacos*5.50
        return valor
        
        
class Muçarela(Pizza): 
    def __init__(self, sabor, qtd_pedacos, bebida):
        super().__init__(sabor, qtd_pedacos, bebida)
        
    def ingredientes_geral (self):
        ingredientes = '\n Ingredientes \n Muçarela \n Molho de tomate \n Tomate \n Orégano \n'
        return ingredientes


class Calabresa(Pizza):
    def __init__(self, sabor, qtd_pedacos, bebida):
        super().__init__(sabor, qtd_pedacos, bebida)

    def ingredientes_geral (self):
        ingredientes = '\n Ingredientes: \n Calabresa \n Muçarela \n Molho de tomate \n Orégano \n Cebola \n'
        return ingredientes    

class Frango(Pizza):
    def __init__(self, sabor, qtd_pedacos, bebida):
        super().__init__(sabor, qtd_pedacos, bebida)

    def ingredientes_geral (self):
        ingredientes = '\n Ingredientes: \n Frango \n Muçarela \n Molho de tomate \n Catupiry \n'
        return ingredientes

class Lombinho(Pizza):
    def __init__(self, sabor, qtd_pedacos, bebida):
        super().__init__(sabor, qtd_pedacos, bebida)

    def ingredientes_geral (self):
        ingredientes = '\n Lombinho \n Muçarela \n Molho de tomate \n Catupiry \n'
        return ingredientes


def main(): 
    pizzas = ['Muçarela', 'Calabresa', 'Frango', 'Lombinho']
    bebidas = ['Coca-cola - 2L', 'Guaraná - 2L', 'Suco de laranja - 1,5L', 'Suco de uva - 1L']
    brindes = ['Chaveiro', 'Caneta', 'Copo', 'Imã de geladeira']
    formatos = ['Redonda(8 pedaços)', 'Quadrada(12 pedaços)', 'Pedaço(1 pedaço)']
    print(f'\n Sabores: \n')
    for index,sabor in enumerate(pizzas,start=1):
        print(f'{index} - {sabor}')
    sabor = input('\nEscolha o sabor:').lower()
    print('\n')
    for pedaco in formatos:
        print(pedaco)
    formato = input('\nEscolha o formato: \n').lower()
    print('\n')
    for index,bebida in enumerate (bebidas,start=1):
        print(f'{index} - {bebida}')
    bebida = input('\nEscolha uma a bebida: \n').lower()
    print('\n')
    for index,brinde in enumerate (brindes,start=1):
        print(f'{index} - {brinde}')
    brinde = input('\nEscolha um brinde: \n').lower()

    

    if formato == 'quadrada':
        qtd_pedacos = 12
        
        
    elif formato == 'redonda':
        qtd_pedacos = 8
        
    
    elif formato == 'pedaço':
        qtd_pedacos = 1

        
    if sabor == 'muçarela' or sabor=='1':
        sabor = 'muçarela'
        pedido = Muçarela(sabor, qtd_pedacos, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
    elif sabor == 'calabresa' or sabor=='2':
        sabor = 'calabresa'
        pedido = Calabresa(sabor, qtd_pedacos, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
        
    elif sabor == 'frango' or sabor=='3':
        pedido = Frango(sabor, qtd_pedacos, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    

    elif sabor == 'lombinho' or sabor=='3':
        pedido = Lombinho(sabor, qtd_pedacos, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
      

    elif sabor == 'marguerita' or sabor=='4':
        pedido = Marguerita(sabor, qtd_pedacos, bebida)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        print(f'O valor total é R$ {pedido.preco_pizza() + pedido.preco_bebida()}')
    
        
       
       
   

    
if __name__ == "__main__":
        main()

    
    

