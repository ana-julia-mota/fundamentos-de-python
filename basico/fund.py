print('Subtração')


numero_x = 3
numero_y = 5 
numero_w = 2.5 
subtracao = int(numero_x) - numero_y - numero_w 
print(subtracao, type(subtracao))
print(f'A subtração de {numero_x} - {numero_y} - {numero_w} é igual a: {subtracao}')
print(f'O tipo de dado é {type(subtracao)} ')


print(' ')

print('Multiplicação')


n_x = 50
n_y = 10
multiplicacao = (n_x) * n_y
print(multiplicacao, type(multiplicacao))
print(f'A multiplicação de {n_x} * {n_y} é igual a : {multiplicacao} ')
print(f'O tipo de dado é {type(multiplicacao)} ')


print(' ')

print('Função built-in >> input() vai ser uma string')
print('Soma')

var1 = int(input('Entre com um número:'))
var2 = int(input('Entre com o segundo número:'))
var3 = int(input('Entre com o terceiro número:'))

soma = int(var1) + var2 + var3
print(f' O valor da soma dos números escolhidos é: {soma} ')

print(' ')

print('Subtração')

num_1 = int(input('Insira um número:'))
num_2 = int(input('Insira o segundo número:'))
num_3 = int(input('Insira o terceiro número '))

subtracao = int(num_1) - num_2 - num_3
print(f'O valor da subtração dos números inseridos é : {subtracao} ')

print(' ')


print('Multiplicação')

n1 = int(input(f'Insira um número:'))
n2 = int(input(f'Insira um número:')) 
n3 = int(input(f'Insira um número:'))

multiplicacao = int(n1) * n2 * n3
print(f'O valor da multiplicação é : {multiplicacao} ')


//operações com strings

nome = 'Ana Júlia'
sobrenome = 'Alves Mota'
ano = 2021

print(f'A junção das informações acima é : {nome} {sobrenome} {ano}')


