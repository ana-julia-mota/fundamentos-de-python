def loop_for ():
    pizzas = ['muçarela', 'calabresa', 'frango', 'lombinho']
    bebidas = ['guaraná', 'coca-cola', 'suco de laranja', 'suco de uva']
    brindes = ['chaveiro', 'caneta', 'copo', 'imã de geladeira']
    print(type(pizzas))
    print('O sabores de pizzas que possuímos são:')
    for pizza in pizzas:
        print(pizza)
    pizzas.append('4 queijos')
    print(pizzas)
    for combo in zip(pizzas, bebidas, brindes):
        print(combo)
    for index,item in enumerate(pizzas, start = 10):
        print(index,item)
    for numero in range(10, 20, 2):
        print(numero)

def return_break_continue ():
    pizzas = ['muçarela', 'calabresa', 'frango', 'lombinho']
    brindes = ['chaveiro', 'caneta', 'copo', 'imã de geladeira']
    for pizza in pizzas:
        if pizza == 'muçarela':
            #int(input("Digite a quantidade de pedaços:"))
            try:
                int(input("Digite a quantidade de pedaços:"))
                continue
            except:
                print("É permitido apenas números.")
        elif pizza != 'muçarela':
            print('Não é muçarela')
            break
        elif pizza == 'calabresa':
            print(pizza)
        elif pizza == 'frango':
            print(pizza)
        if pizza == 'lombinho':
            print(pizza)
        else: 
            print('Sabor não encontrado.')
    return pizzas
def loop_while ():
    pizzas = ['muçarela', 'calabresa', 'frango', 'lombinho']
    contador = 0
    while contador <= 20:
        print(contador)
        contador = contador + 1
    while contador < 100:
        dominio = 'https://www.gov.br/pt-br/noticias/ultimas-noticias?b_start:int='
        dominio = dominio + str(contador)
        contador = contador + 10
        print(dominio)



def main ():
    #exemplo_loop_for = loop_for()
    #loop_while()
    exemplo_return = return_break_continue()
    print(exemplo_return)

if __name__ == "__main__": 
    main ()

''' Tarefa:
For para as bebidas e os brindes com f 
Colocar na Aula_04'''