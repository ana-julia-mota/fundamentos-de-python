# TAREFA:  
#1. Incluir multiplicar e dividir

class CalculadoraSimples:
    def __init__(self, num_x, num_y):
        self.num_x = num_x
        self.num_y = num_y

    def multiplicar(self): 
        return f'A multiplicação de {self.num_x} * {self.num_y} é igual a: {self.num_x * self.num_y}'
        
    def dividir(self):
        return f'A divisão de {self.num_x} / {self.num_y} = {self.num_x / self.num_y}'
 
def main ():
    num_x, num_y = 10,5
    valores = CalculadoraSimples(num_x, num_y)
    print(valores.multiplicar())
    print(valores.dividir())

if __name__ == "__main__": 
    main ()


#2. Utilizar o input para os números

num_x = int(input('Digite um número: '))
num_y = int(input('Digite um número: '))

class CalculadoraSimples:
    def __init__(self, num_x, num_y):
        self.num_x = num_x
        self.num_y = num_y
        
    def somar(self):
        return f'A soma de {self.num_x} + {self.num_y} é igual a: {self.num_x + self.num_y}'
        
    def subtrair(self):
        return f'A subtração de {self.num_x} - {self.num_y} é igual a: {self.num_x - self.num_y}'

    def multiplicar(self): 
        return f'A multiplicação de {self.num_x} * {self.num_y} é igual a: {self.num_x * self.num_y}'
        
    def dividir(self):
        return f'A divisão de {self.num_x} / {self.num_y} = {self.num_x / self.num_y}'
 
def main ():
    valores = CalculadoraSimples(num_x, num_y)
    print(valores.somar())
    print(valores.subtrair())
    print(valores.multiplicar())
    print(valores.dividir())

if __name__ == "__main__": 
    main ()


'''3. Criar uma classe chamada "pizza". 
    a. incluir como atributos a quantidade de pedaços que você quer e o sabor;
    b. os métodos serão mussarela e calabresa ou qualquer outro sabor
    c. retornar os ingredientes e o valor da pizza'''

    



